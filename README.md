# README #

Welcome to pyservo

### What is this repository for? ###

* A set of example python programs for using a servo from the Raspberry Pi using GPIO

### How do I get set up? ###

* Clone this repo `$ git clone {this repo}`
* `$ cd pyservo`
* `$ sudo python sweep.py`
* Explore and try the other programs in here!


### Who do I talk to? ###

* Owner: Dan Toraason `email: dan@toraason.com`
