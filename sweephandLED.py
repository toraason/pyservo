# Servo control - from learn.adafruit.com
import time
import wiringpi

# use 'GPIO naming'
wiringpi.wiringPiSetupGpio()

SERVO = 18
LED = 12

# set GPIO 18 to be a PWM output
wiringpi.pinMode(SERVO,wiringpi.GPIO.PWM_OUTPUT)
# set GPIO 12 to be a digital (on/off) output
wiringpi.pinMode(LED, 1)	

# set the PWM mode to milliseconds stype
wiringpi.pwmSetMode(wiringpi.GPIO.PWM_MODE_MS)

# divide down clock
wiringpi.pwmSetClock(192)
wiringpi.pwmSetRange(2000)

delay_period = 0.01

try:
	while True:
		for pulse in range(50,150,1):
			wiringpi.pwmWrite(SERVO, pulse)
			time.sleep(delay_period)
		wiringpi.digitalWrite(LED, 1)
		time.sleep(delay_period)
		wiringpi.digitalWrite(LED, 0)
		for pulse in range(149,51,-1):
			wiringpi.pwmWrite(SERVO, pulse)
			time.sleep(delay_period)

except:
	for pulse in range(pulse,50,-1):
			wiringpi.pwmWrite(SERVO,pulse)
			time.sleep(delay_period)
	wiringpi.digitalWrite(LED,0)
